#!/bin/bash
cd /opt/inmobiliaria
source venv/bin/activate
cd /opt/inmobiliaria/inmobiliaria
gunicorn inmobiliaria.wsgi -t 600 -b 127.0.0.1:8001 -w 6 --user=servidor --group=servidor --log-file=/opt/inmobiliaria/gunicorn.log 2>>/opt/inmobiliaria/gunicorn.log

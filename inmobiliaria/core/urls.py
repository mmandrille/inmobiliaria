from django.conf.urls import url
from django.urls import path
#Import personales
from . import views

app_name = 'core'
urlpatterns = [
    #Basicas:
    url(r'^$', views.home, name='home'),

    path('nosotros/', views.nosotros, name='nosotros'),
    path('contacto/', views.contacto, name='contacto'),

    path('propiedades/', views.propiedades, name='propiedades'),
    path('propiedades/<int:prop_id>/', views.propiedad, name='propiedad'),
    
    path('buscador/', views.buscador, name='buscador'),

    path('tasaciones/', views.tasaciones, name='tasaciones'),
    path('proyectos/', views.proyectos, name='proyectos'),
    #Busquedas simplificadas
    path('ventas/', views.ventas, name='ventas'),
    path('alquileres/', views.alquileres, name='alquileres'),
]
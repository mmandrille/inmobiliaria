#Modulos Standards
from django.shortcuts import render
from django.db.models import Q

#Import Propios
from .models import TIPO_NEGOCIO, TipoPropiedad, Etiqueta, Zona
from .models import Propiedad, Foto
from .models import Sede
from .functions import obtener_propiedades
from .forms import TasacionForm, ProyectoForm

# Create your views here.
def home(request):
    props = obtener_propiedades(4)
    return render(request, 'home.html', {'propiedades': props, })

def nosotros(request):
    return render(request, 'nosotros.html', { })

def contacto(request):
    sedes = Sede.objects.all()
    return render(request, 'contacto.html', {'sedes': sedes, })

def propiedades(request, props=None):
    tipos_propiedad = TipoPropiedad.objects.all()
    zonas = Zona.objects.all()
    etiquetas = Etiqueta.objects.all()
    if not props:
        props = []
        if request.method == 'POST':
            if request.POST.get('search_box'): #En caso de que se haya realizado una busqueda
                texto = request.POST.get('search_box')
                props = Propiedad.objects.filter(
                    Q(nombre__icontains=texto)|
                    Q(descripcion__icontains=texto)).distinct()
            elif 'filtrar' in request.POST:#Si se uso el sistema de filtros
                props = obtener_propiedades()#buscamos todas las propiedades
                #Filtramos por tipo de negocio
                filtro = []
                for tn in TIPO_NEGOCIO:
                    if "tn-"+str(tn[0]) in request.POST:
                        filtro.append(tn[0])
                if filtro:
                    props = props.filter(tipo_negocio__in=filtro)
                #Filtramos por tipo de propiedad
                filtro = []
                for tp in tipos_propiedad:
                    if "tp-"+str(tp.id) in request.POST:
                        filtro.append(tp)
                if filtro:
                    props = props.filter(tipo_propiedad__in=filtro)
                #Filtramos por zonas
                filtro = []
                for zona in zonas:
                    if "zona-"+str(zona.id) in request.POST:
                        filtro.append(zona)
                if filtro:
                    props = props.filter(zonas__in=filtro)
                #Filtramos por etiquetas
                filtro = []
                for etiqueta in etiquetas:
                    if "etiqueta-"+str(etiqueta.id) in request.POST:
                        filtro.append(etiqueta)
                if filtro:
                    props = props.filter(etiquetas__in=filtro)
            props = props.filter(activa=True)
        else:
            props = obtener_propiedades(12)
    return render(request, 'propiedades.html', {'propiedades': props, 'tipos_negocio': TIPO_NEGOCIO, 'tipos_propiedad': tipos_propiedad, 'etiquetas': etiquetas, 'zonas': zonas,})

def propiedad(request, prop_id):
    prop = Propiedad.objects.get(pk=prop_id)
    return render(request, 'propiedad.html', {'prop': prop, })

def buscador(request):
    tipos_propiedad = TipoPropiedad.objects.all()
    zonas = Zona.objects.all()
    etiquetas = Etiqueta.objects.all()
    return render(request, 'filtrador.html', {'tipos_negocio': TIPO_NEGOCIO, 'tipos_propiedad': tipos_propiedad, 'etiquetas': etiquetas, 'zonas': zonas,})

def ventas(request):
    props = Propiedad.objects.filter(tipo_negocio=1, activa=True)
    return propiedades(request, props)

def alquileres(request):
    props = Propiedad.objects.filter(tipo_negocio=2, activa=True)
    return propiedades(request, props)

def tasaciones(request):
    enviado = False
    if request.method == 'POST':
        form = TasacionForm(request.POST)
        if form.is_valid():
            form.save()
            enviado = True
    else:
        form = TasacionForm()
    return render(request, 'tasaciones.html', {'form': form, 'enviado': enviado})

def proyectos(request):
    enviado = False
    if request.method == 'POST':
        form = ProyectoForm(request.POST)
        if form.is_valid():
            form.save()
            enviado = True
    else:
        form = ProyectoForm()
    return render(request, 'proyectos.html', {'form': form, 'enviado': enviado})
from django.contrib import admin
#Modulos para no permitir escalacion de privilegios
from django.contrib.auth.admin import UserAdmin, User
#Import Personales
from .models import Moneda
from .models import TipoPropiedad, Etiqueta, Servicio, Zona
from .models import Propiedad, Foto
from .models import Sede, Contacto
from .models import Tasacion, Proyecto

#Modificacion del panel de administrador para no permitir escalacion de privilegios
class RestrictedUserAdmin(UserAdmin):
    model = User
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(RestrictedUserAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        user = kwargs['request'].user
        if not user.is_superuser:
            if db_field.name == 'groups':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.groups.all()])
            if db_field.name == 'user_permissions':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.user_permissions.all()])
            if db_field.name == 'is_superuser':
                field.widget.attrs['disabled'] = True
        return field

# Register your models here.
class FotoInline(admin.TabularInline):
    model = Foto

class PropiedadesAdmin(admin.ModelAdmin):
    list_filter = ['tipo_negocio', 'tipo_propiedad', 'zonas', 'servicios', 'etiquetas']
    inlines = [FotoInline]
    ordering = ['fecha']
    search_fields = ['nombre']

class ContactoInline(admin.TabularInline):
    model = Contacto

class SedeAdmin(admin.ModelAdmin):
    inlines = [ContactoInline]
    search_fields = ['nombre']

class TasacionAdmin(admin.ModelAdmin):
    list_filter = ['barrio', 'tipo_propiedad']
    ordering = ['fecha_consulta']
    search_fields = ['barrio', 'info_adicional']

class ProyectoAdmin(admin.ModelAdmin):
    ordering = ['fecha_consulta']
    search_fields = ['info_adicional']

#Registramos modificaciones para no permitir escalacion de privilegios
admin.site.unregister(User)
admin.site.register(User, RestrictedUserAdmin)
# Register your models here.
admin.site.register(Moneda, )
admin.site.register(TipoPropiedad, )
admin.site.register(Etiqueta, )
admin.site.register(Servicio, )
admin.site.register(Zona, )
admin.site.register(Propiedad, PropiedadesAdmin)
admin.site.register(Sede, SedeAdmin)
admin.site.register(Tasacion, TasacionAdmin)
admin.site.register(Proyecto, ProyectoAdmin)

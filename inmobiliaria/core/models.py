#Import Genericos
from __future__ import unicode_literals
import datetime
from django.db import models

#Modulos extras:
from tinymce.models import HTMLField

#Choice Options
TIPO_NEGOCIO = (
    (1, 'Venta'),
    (2, 'Alquiler'),
    
    (9, 'Otro'),
)

TIPO_CONDICION = (
    (1, 'Excelente'),
    (2, 'Muy Buena'),
    (3, 'Buena'),
    (4, 'Mala'),

    (9, 'Otra'),
)

# Create your models here.
class TipoPropiedad(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    def __str__(self):
        return self.nombre

class Moneda(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    simbolo = models.CharField('Simbolo', max_length=5)
    def __str__(self):
        return self.nombre

class Etiqueta(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    def __str__(self):
        return self.nombre

class Zona(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    def __str__(self):
        return self.nombre

class Servicio(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    def __str__(self):
        return self.nombre

class Propiedad(models.Model):
    tipo_negocio = models.IntegerField(choices=TIPO_NEGOCIO, default=1)
    tipo_propiedad = models.ForeignKey(TipoPropiedad, on_delete=models.CASCADE, related_name="propiedades")
    nombre = models.CharField('Nombre', max_length=200)
    fecha = models.DateTimeField('Fecha de Publicacion', default=datetime.datetime.now)
    tmoneda = models.ForeignKey(Moneda, on_delete=models.CASCADE, null=True, blank=True, related_name="propiedades")
    precio = models.IntegerField('Precio', default=0)
    descripcion = HTMLField(null=True, blank=True)
    zonas = models.ManyToManyField(Zona, related_name="propiedades")
    direccion = models.CharField('Direccion', max_length=200)
    gps = models.URLField('Gmap Link', blank=True, null=True)
    dimensiones = models.CharField('Dimensiones', max_length=200)
    etiquetas = models.ManyToManyField(Etiqueta, blank=True, related_name="propiedades")
    servicios = models.ManyToManyField(Servicio, blank=True, related_name="propiedades")
    activa = models.BooleanField('Activa', default=True)
    class Meta:
        verbose_name_plural = 'Propiedades'
    def __str__(self):
        return self.nombre

class Foto(models.Model):
    propiedad = models.ForeignKey(Propiedad, on_delete=models.CASCADE, related_name="fotos")
    orden = models.IntegerField(default=0)
    epigrafe = models.CharField('Nombre', max_length=25)
    imagen = models.FileField('Imagen', upload_to='propiedades/', null=True, blank=True)
    def __str__(self):
        return self.propiedad.nombre + str(self.orden)

class Sede(models.Model):
    nombre = models.CharField('Nombre', max_length=200)
    direccion = models.CharField('Direccion', max_length=200)
    localidad = models.CharField('Localidad', max_length=200)
    horario = models.CharField('Horario de Atencion', max_length=200)
    def __str__(self):
        return self.nombre

class Contacto(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, related_name="contactos")
    nombre = models.CharField('Nombre', max_length=200)
    telefono = models.CharField('Telefono', max_length=100)
    def __str__(self):
        return self.sede.nombre + ': ' + self.nombre

class Proyecto(models.Model):
    autor = models.CharField('Nombre y Apellido', max_length=100)
    email = models.EmailField('Correo Electronico Personal')
    telefono = models.CharField('Telefono', max_length=50, blank=True, null=True)
    info_adicional = HTMLField()
    fecha_consulta = models.DateTimeField(auto_now_add=True)
    valida = models.BooleanField('Email Validado', default=False)
    respondida = models.BooleanField('Ya fue Respondida', default=False)
    def __str__(self):
        return u"%s por %s" % (self.email, self.info_adicional[:50])

class Tasacion(models.Model):
    autor = models.CharField('Nombre y Apellido', max_length=100)
    email = models.EmailField('Correo Electronico Personal')
    telefono = models.CharField('Telefono', max_length=50, blank=True, null=True)
    barrio = models.CharField('Barrio', max_length=100)
    calle = models.CharField('Calle', max_length=100)
    numeracion = models.CharField('Numeracion', max_length=100)
    tipo_propiedad = models.ForeignKey(TipoPropiedad, on_delete=models.CASCADE, related_name="tasaciones")
    metros_cubiertos = models.IntegerField('Mts2 Cubiertos', default=0)
    metros_semicubiertos = models.IntegerField('Mts2 SemiCubiertos', default=0)
    metros_descubiertos = models.IntegerField('Mts2 Descubiertos', default=0)
    antiguedad = models.IntegerField('Antiguedad', default=0)
    tipo_tasacion = models.IntegerField(choices=TIPO_NEGOCIO, default=1)
    condiciones = models.IntegerField(choices=TIPO_CONDICION, default=1)
    info_adicional = HTMLField()
    fecha_consulta = models.DateTimeField(auto_now_add=True)
    valida = models.BooleanField('Email Validado', default=False)
    respondida = models.BooleanField('Ya fue Respondida', default=False)
    def __str__(self):
        return u"%s por %s de %s en %s" % (self.email, self.get_tipo_tasacion_display(), self.tipo_propiedad.nombre, self.barrio)

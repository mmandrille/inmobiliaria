#Import Standards

#Imports del proyecto
from .models import Propiedad

#Definimos funciones propias
def obtener_propiedades(cant=None):
    props = Propiedad.objects.all().order_by('-fecha')
    props = props.filter(activa=True)
    props = props.select_related('tipo_propiedad', 'tmoneda')
    props = props.prefetch_related('zonas', 'etiquetas', 'servicios', 'fotos')
    if cant:
        props = props[:cant]
    return props
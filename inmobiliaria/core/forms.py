#Import Standard
from django import forms
from django.forms import ModelForm
from django.db import models

#Import del Proyecto
from .models import Tasacion, Proyecto

#Definimos nuestros Formularios

class TasacionForm(ModelForm):
    class Meta:
        model = Tasacion
        fields = '__all__'
        widgets = {
                    'valida': forms.HiddenInput(),
                    'respondida': forms.HiddenInput()}

class ProyectoForm(ModelForm):
    class Meta:
        model = Proyecto
        fields = '__all__'
        widgets = {
                    'valida': forms.HiddenInput(),
                    'respondida': forms.HiddenInput()}
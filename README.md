# inmobiliaria

Sistema web institucional + presentacion de propiedades + consultas
Django2+

Para poder probar este proyecto solo deben Instalar:
- Python3+

Correr:
- pip install -r requeriments.txt
- python3 manage.py makemigrations
- python3 manage.py migrate
- python3 manage.py createsuruser
- python3 manage.py runserver

Podran accederlo:
- Home: http://localhost:8000
- Admin: http://localhost:8000/admin